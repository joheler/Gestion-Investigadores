/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.SubtipoProductoDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class SubtipoProductoDAO {

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList listarSubtipoProducto(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList listado = null;
        SubtipoProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT supr_id, supr_descripcion FROM subtipo_producto ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new SubtipoProductoDTO();
                datos.setId(rs.getString("supr_id"));
                datos.setDescripcion(rs.getString("supr_descripcion"));

                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }

        return listado;
    }
}
