/*
<<<<<<< HEAD
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
=======
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
>>>>>>> 5b039bebb05e33e6af774c4397a1839cc847e0f7
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;

import co.usb.gestion.mvc.dto.FormacionAcademicalDTO;
import co.usb.gestion.mvc.dto.PerfilDTO;
import co.usb.gestion.mvc.dto.PerfilProductoDTO;
import co.usb.gestion.mvc.dto.UsuarioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class PerfilProductoDAO {

    /**
     *
     * @param conexion
     * @param Datos
     * @return
     */
    public boolean registrarPerfilProducto(Connection conexion, PerfilProductoDTO Datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO perfil_producto (supr_id, perf_id, supr_nombre, supr_descripcion, supr_fecha) ");
            cadSQL.append(" VALUES (?, ?, ?, ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, Datos.getIdSubtipoProducto(), ps);
            AsignaAtributoStatement.setString(2, Datos.getIdPerfil(), ps);
            AsignaAtributoStatement.setString(3, Datos.getNombre(), ps);
            AsignaAtributoStatement.setString(4, Datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(5, Datos.getFecha(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    Datos.setIdPerfilProducto(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarPerfilProducto(Connection conexion, PerfilProductoDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            System.out.println(datos);

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE perfil_producto SET supr_id = ?, perf_id = ?, supr_nombre = ?,"
                    + " supr_descripcion = ?, supr_fecha = ? WHERE pepr_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getIdSubtipoProducto(), ps);
            AsignaAtributoStatement.setString(2, datos.getIdPerfil(), ps);
            AsignaAtributoStatement.setString(3, datos.getNombre(), ps);
            AsignaAtributoStatement.setString(4, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(5, datos.getFecha(), ps);
            AsignaAtributoStatement.setString(6, datos.getIdPerfilProducto(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<PerfilProductoDTO> consultarSubtipo(Connection conexion, String condicion, String perfil) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilProductoDTO> listado = null;
        PerfilProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();

            cadSQL.append(" SELECT pp.pepr_id,pp.supr_id,sp.supr_descripcion,pp.supr_nombre,pp.supr_descripcion,pp.supr_fecha");
            cadSQL.append("  FROM perfil p INNER JOIN perfil_producto pp ON p.perf_id = pp.perf_id");
            cadSQL.append(" INNER JOIN subtipo_producto sp ON pp.supr_id = sp.supr_id");
            cadSQL.append("  INNER JOIN tipo_producto tp ON sp.tipr_id = tp.tipr_id");
            cadSQL.append("  INNER JOIN clase_producto cp ON sp.supr_id = cp.supr_id");
            cadSQL.append("  WHERE p.perf_id = " + perfil + " GROUP BY 1");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilProductoDTO();
                datos.setIdPerfilProducto(rs.getString("pp.pepr_id"));
                datos.setIdSubtipoProducto(rs.getString("pp.supr_id"));
                datos.setDescripcionSubtipoProd(rs.getString("sp.supr_descripcion"));
                datos.setNombre(rs.getString("pp.supr_nombre"));
                datos.setDescripcion(rs.getString("pp.supr_descripcion"));
                datos.setFecha(rs.getString("pp.supr_fecha"));
                listado.add(datos);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param idPerfil
     * @return
     */
    public ArrayList<PerfilProductoDTO> ConsultarPerfilProducto(Connection conexion, String idPerfil) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilProductoDTO> listado = null;
        PerfilProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT pepr.pepr_id, pepr.supr_id, pepr.perf_id, pepr.supr_nombre, pepr.supr_descripcion, pepr.supr_fecha, ");
            cadSQL.append(" supr.supr_descripcion, supr.supr_estado, ");
            cadSQL.append(" tipr.tipr_id, tipr.tipr_descripcion, tipr.tipr_estado ");
            cadSQL.append(" FROM perfil_producto as pepr ");
            cadSQL.append(" INNER JOIN subtipo_producto AS supr ON pepr.supr_id = supr.supr_id");
            cadSQL.append(" INNER JOIN tipo_producto AS tipr ON supr.tipr_id = tipr.tipr_id ");
            cadSQL.append(" WHERE pepr.perf_id = ?");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idPerfil, ps);

            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilProductoDTO();
                datos.setIdPerfilProducto(rs.getString("pepr.pepr_id"));
                datos.setIdSubtipoProducto(rs.getString("pepr.supr_id"));
                datos.setIdPerfil(rs.getString("pepr.perf_id"));
                datos.setNombre(rs.getString("pepr.supr_nombre"));
                datos.setDescripcion(rs.getString("pepr.supr_descripcion"));
                datos.setFecha(rs.getString("pepr.supr_fecha"));
                datos.setDescripcionSubtipoProd(rs.getString("supr.supr_descripcion"));
                datos.setEstadoSubtipoProd(rs.getString("supr.supr_estado"));
                datos.setIdTipoProducto(rs.getString("tipr.tipr_id"));
                datos.setDescripcionTipoProd(rs.getString("tipr.tipr_descripcion"));
                datos.setEstadoTipoProd(rs.getString("tipr.tipr_estado"));
                listado.add(datos);
            }

            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
}
