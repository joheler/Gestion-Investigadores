/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.NivelFormacionDTO;
import co.usb.gestion.mvc.dto.TipoInvestigadorDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class NivelFormacionDAO {

    public ArrayList<NivelFormacionDTO> listarNivelFormacion(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<NivelFormacionDTO> listado = null;
        NivelFormacionDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT nifo_id, nifo_descripcion, nifo_estado FROM nivel_formacion ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new NivelFormacionDTO();
                datos.setId(rs.getString("nifo_id"));
                datos.setDescripcion(rs.getString("nifo_descripcion"));
                datos.setEstado(rs.getString("nifo_estado"));
                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

}
