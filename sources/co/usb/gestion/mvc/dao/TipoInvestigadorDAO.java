/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.TipoInvestigadorDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class TipoInvestigadorDAO {

    public ArrayList<TipoInvestigadorDTO> listarTipoInvestigador(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<TipoInvestigadorDTO> listado = null;
        TipoInvestigadorDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT tiin_id, tiin_descripcion, tiin_estado FROM tipo_investigador ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new TipoInvestigadorDTO();
                datos.setId(rs.getString("tiin_id"));
                datos.setDescripcion(rs.getString("tiin_descripcion"));
                datos.setEstado(rs.getString("tiin_estado"));
                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

}
