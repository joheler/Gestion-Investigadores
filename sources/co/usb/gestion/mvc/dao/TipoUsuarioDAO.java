/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.TipoUsuarioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class TipoUsuarioDAO {

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList listarTipoUsuario(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList listado = null;
        TipoUsuarioDTO datosTipoUsuario = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT tius_id, tius_descripcion FROM tipo_usuario ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datosTipoUsuario = new TipoUsuarioDTO();
                datosTipoUsuario.setId(rs.getString("tius_id"));
                datosTipoUsuario.setDescripcion(rs.getString("tius_descripcion"));

                listado.add(datosTipoUsuario);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }

        return listado;
    }
}
