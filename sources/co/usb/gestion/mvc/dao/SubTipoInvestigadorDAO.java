/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.SubTipoInvestigadorDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class SubTipoInvestigadorDAO {

    /**
     * @param idTipoInv
     * @return
     */
    public ArrayList<SubTipoInvestigadorDTO> listarSubTipoInvestigador(Connection conexion, String idTipoInv) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList listado = null;
        SubTipoInvestigadorDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT suin_id, tiin_id, suin_codigo, suin_descripcion, suin_estado FROM subtipo_investigador WHERE  tiin_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idTipoInv, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new SubTipoInvestigadorDTO();
                datos.setId(rs.getString("suin_id"));
                datos.setIdTipoInvestigador(rs.getString("tiin_id"));
                datos.setIdTipoInvestigador(rs.getString("suin_codigo"));
                datos.setDescripcion(rs.getString("suin_descripcion"));
                datos.setEstado(rs.getString("suin_estado"));
                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }

        return listado;
    }

}
