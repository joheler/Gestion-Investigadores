/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.fachada;

import co.usb.gestion.mvc.dto.FormacionAcademicalDTO;
import co.usb.gestion.mvc.dto.NivelFormacionDTO;
import co.usb.gestion.mvc.dto.PerfilDTO;
import co.usb.gestion.mvc.dto.PerfilProductoDTO;
import co.usb.gestion.mvc.dto.SubTipoInvestigadorDTO;
import co.usb.gestion.mvc.dto.SubtipoProductoDTO;
import co.usb.gestion.mvc.dto.TipoInvestigadorDTO;
import co.usb.gestion.mvc.dto.TipoUsuarioDTO;
import co.usb.gestion.mvc.dto.UsuarioDTO;
import co.usb.gestion.mvc.mediador.MediadorAppGestion;
import java.util.ArrayList;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.directwebremoting.annotations.ScriptScope;

/**
 *
 * @author Sys. Hebert Medelo
 */
@RemoteProxy(name = "ajaxGestion", scope = ScriptScope.SESSION)
public class FachadaAppGestion {

    /**
     *
     */
    public FachadaAppGestion() {
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public boolean servicioActivo() {
        return true;
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * LOS METODOS APARTIR DE AQUI NO HAN SIDO VALIDADOS
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     */
    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<TipoUsuarioDTO> listarTipoUsuario() {
        return MediadorAppGestion.getInstancia().listarTipoUsuario();
    }

    /**
     * @param idTipoInv
     * @return
     */
    @RemoteMethod
    public ArrayList<SubTipoInvestigadorDTO> listarSubTipoInvestigador(String idTipoInv) {
        return MediadorAppGestion.getInstancia().listarSubTipoInvestigador(idTipoInv);
    }

    /**
     * @param usuario
     * @param perfil
     * @return
     */
    @RemoteMethod
    public boolean registrarUsuario(UsuarioDTO usuario, PerfilDTO perfil) {
        return MediadorAppGestion.getInstancia().registrarUsuario(usuario, perfil);
    }

    /**
     * @param condicion
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilDTO> consultarPerfilPorNombreDocumento(String condicion) {
        return MediadorAppGestion.getInstancia().consultarPerfilPorNombreDocumento(condicion);
    }

    /**
     *
     * @param datosUsuario
     * @return
     */
    @RemoteMethod
    public boolean actualizarEstadoUsuario(UsuarioDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarEstadoUsuario(datos);
    }

    /**
     *
     * @param documento
     * @return
     */
    public UsuarioDTO recuperarContrasenia(String documento) {
        return MediadorAppGestion.getInstancia().recuperarContrasenia(documento);
    }
    
     /**
     * @param condicion
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilDTO> consultarGestionPerfilProducto(String condicion) {
        return MediadorAppGestion.getInstancia().consultarGestionPerfilProducto(condicion);
    }
    
     /**
     * @param idPerfil
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilProductoDTO> ConsultarPerfilProducto(String idPerfil) {
        return MediadorAppGestion.getInstancia().ConsultarPerfilProducto(idPerfil);
    }    

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<TipoInvestigadorDTO> listarTipoInvestigador() {
        return MediadorAppGestion.getInstancia().listarTipoInvestigador();
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<NivelFormacionDTO> listarNivelFormacion() {
        return MediadorAppGestion.getInstancia().listarNivelFormacion();
    }
    
    /**
     * @param formacion
     * @return
     */
    @RemoteMethod
    public boolean registrarInfAcademica(FormacionAcademicalDTO formacion) {
        return MediadorAppGestion.getInstancia().registrarInfAcademica(formacion);
    }
    
    /**
     * 
     * @param datos
     * @return 
     */
     @RemoteMethod
    public boolean actualizarFormacionAcad(FormacionAcademicalDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarFormacionAcad(datos);
    }

    
    /**
     * @param condicion
     * @return
     */
    @RemoteMethod
    public ArrayList<FormacionAcademicalDTO> consultarFormacionPorInstitucionPrograma(String condicion) {
        return MediadorAppGestion.getInstancia().consultarFormacionPorInstitucionPrograma(condicion);
    }

    /**
     * @param usuario
     * @param perfil
     * @return
     */
    @RemoteMethod
    public boolean actualizarPerfil(PerfilDTO perfil, UsuarioDTO usuario) {
        return MediadorAppGestion.getInstancia().actualizarPerfil(perfil, usuario);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilDTO> listarUsuarios() {
        return MediadorAppGestion.getInstancia().listarUsuarios();
    }
    
    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<SubtipoProductoDTO> listarSubtipoProducto() {
        return MediadorAppGestion.getInstancia().listarSubtipoProducto();
    }
    
    /**
     * @param Datos
     * @return
     */
    @RemoteMethod
    public boolean registrarPerfilProducto(PerfilProductoDTO Datos) {
        return MediadorAppGestion.getInstancia().registrarPerfilProducto(Datos);
    }
    
     @RemoteMethod
    public boolean actualizarPerfilProducto(PerfilProductoDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarPerfilProducto(datos);
    }

    
    /**
     * @param condicion
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilProductoDTO> consultarSubtipo(String condicion) {
        return MediadorAppGestion.getInstancia().consultarSubtipo(condicion);
    }

}
