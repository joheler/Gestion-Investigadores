/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usb.gestion.mvc.dto;

import co.usb.gestion.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;

/**
 *
 * @author carlos
 */
public class PerfilProductoDTO implements Serializable {

    String idPerfilProducto = Generales.EMPTYSTRING;
    String idSubtipoProducto = Generales.EMPTYSTRING;
    String idPerfil = Generales.EMPTYSTRING;
    String nombre = Generales.EMPTYSTRING;
    String descripcion = Generales.EMPTYSTRING;
    String fecha = Generales.EMPTYSTRING;
    String descripcionSubtipoProd = Generales.EMPTYSTRING;
    String estadoSubtipoProd = Generales.EMPTYSTRING;
    String descripcionTipoProd = Generales.EMPTYSTRING;
    String estadoTipoProd = Generales.EMPTYSTRING;
    String idTipoProducto = Generales.EMPTYSTRING;

    public String getIdPerfilProducto() {
        return idPerfilProducto;
    }

    public void setIdPerfilProducto(String idPerfilProducto) {
        this.idPerfilProducto = idPerfilProducto;
    }

    public String getIdSubtipoProducto() {
        return idSubtipoProducto;
    }

    public void setIdSubtipoProducto(String idSubtipoProducto) {
        this.idSubtipoProducto = idSubtipoProducto;
    }

    public String getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcionSubtipoProd() {
        return descripcionSubtipoProd;
    }

    public void setDescripcionSubtipoProd(String descripcionSubtipoProd) {
        this.descripcionSubtipoProd = descripcionSubtipoProd;
    }

    public String getEstadoSubtipoProd() {
        return estadoSubtipoProd;
    }

    public void setEstadoSubtipoProd(String estadoSubtipoProd) {
        this.estadoSubtipoProd = estadoSubtipoProd;
    }

    public String getDescripcionTipoProd() {
        return descripcionTipoProd;
    }

    public void setDescripcionTipoProd(String descripcionTipoProd) {
        this.descripcionTipoProd = descripcionTipoProd;
    }

    public String getEstadoTipoProd() {
        return estadoTipoProd;
    }

    public void setEstadoTipoProd(String estadoTipoProd) {
        this.estadoTipoProd = estadoTipoProd;
    }

    public String getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(String idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
