/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dto;

import co.usb.gestion.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class PerfilDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String nombre = Generales.EMPTYSTRING;
    String apellido = Generales.EMPTYSTRING;
    String documento = Generales.EMPTYSTRING;
    String direccion = Generales.EMPTYSTRING;
    String telefono = Generales.EMPTYSTRING;
    String correo = Generales.EMPTYSTRING;
    String sexo = Generales.EMPTYSTRING;
    String idUsuario = Generales.EMPTYSTRING;
    String idSubtipoInvesstigador = Generales.EMPTYSTRING;
    String idTipoUsuario = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
    String tipoUsuario = Generales.EMPTYSTRING;
    String usuario = Generales.EMPTYSTRING;
    String idFormacionAcademica = Generales.EMPTYSTRING;
    String anioInicioFA = Generales.EMPTYSTRING;
    String anioGraduacionFA = Generales.EMPTYSTRING;
    String institucionFA = Generales.EMPTYSTRING;
    String programaAcademicoFA = Generales.EMPTYSTRING;
    String idNivelFormacion = Generales.EMPTYSTRING;
    String descripcionNivelFo = Generales.EMPTYSTRING;
    String estadoNivelFo = Generales.EMPTYSTRING;
    
    ArrayList<MenuDTO> menu = new ArrayList<>();
    String idPerfil = Generales.EMPTYSTRING; 
    String idTipoInvestigador = Generales.EMPTYSTRING;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdSubtipoInvesstigador() {
        return idSubtipoInvesstigador;
    }

    public void setIdSubtipoInvesstigador(String idSubtipoInvesstigador) {
        this.idSubtipoInvesstigador = idSubtipoInvesstigador;
    }

    public String getIdTipoUsuario() {
        return idTipoUsuario;
    }

    public void setIdTipoUsuario(String idTipoUsuario) {
        this.idTipoUsuario = idTipoUsuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public ArrayList<MenuDTO> getMenu() {
        return menu;
    }

    public void setMenu(ArrayList<MenuDTO> menu) {
        this.menu = menu;
    }

    public String getIdFormacionAcademica() {
        return idFormacionAcademica;
    }

    public void setIdFormacionAcademica(String idFormacionAcademica) {
        this.idFormacionAcademica = idFormacionAcademica;
    }

    public String getAnioInicioFA() {
        return anioInicioFA;
    }

    public void setAnioInicioFA(String anioInicioFA) {
        this.anioInicioFA = anioInicioFA;
    }

    public String getAnioGraduacionFA() {
        return anioGraduacionFA;
    }

    public void setAnioGraduacionFA(String anioGraduacionFA) {
        this.anioGraduacionFA = anioGraduacionFA;
    }

    public String getInstitucionFA() {
        return institucionFA;
    }

    public void setInstitucionFA(String institucionFA) {
        this.institucionFA = institucionFA;
    }

    public String getProgramaAcademicoFA() {
        return programaAcademicoFA;
    }

    public void setProgramaAcademicoFA(String programaAcademicoFA) {
        this.programaAcademicoFA = programaAcademicoFA;
    }

    public String getIdNivelFormacion() {
        return idNivelFormacion;
    }

    public void setIdNivelFormacion(String idNivelFormacion) {
        this.idNivelFormacion = idNivelFormacion;
    }

    public String getDescripcionNivelFo() {
        return descripcionNivelFo;
    }

    public void setDescripcionNivelFo(String descripcionNivelFo) {
        this.descripcionNivelFo = descripcionNivelFo;
    }

    public String getEstadoNivelFo() {
        return estadoNivelFo;
    }

    public void setEstadoNivelFo(String estadoNivelFo) {
        this.estadoNivelFo = estadoNivelFo;
    }

    public String getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getIdTipoInvestigador() {
        return idTipoInvestigador;
    }

    public void setIdTipoInvestigador(String idTipoInvestigador) {
        this.idTipoInvestigador = idTipoInvestigador;
    }

    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
