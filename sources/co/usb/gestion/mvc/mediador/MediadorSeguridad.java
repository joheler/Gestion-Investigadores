/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.mediador;

import co.usb.gestion.common.connection.ContextDataResourceNames;
import co.usb.gestion.common.connection.DataBaseConnection;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dao.PerfilDAO;
import co.usb.gestion.mvc.dao.FuncionalidadDAO;
import co.usb.gestion.mvc.dao.MenuDAO;
import co.usb.gestion.mvc.dto.PerfilDTO;
import co.usb.gestion.mvc.dto.FuncionalidadDTO;
import co.usb.gestion.mvc.dto.MenuDTO;
import java.sql.Connection;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class MediadorSeguridad {

    /**
     *
     */
    private final static MediadorSeguridad instancia = null;
    private final LoggerMessage logMsg;

    /**
     *
     */
    public MediadorSeguridad() {
        logMsg = LoggerMessage.getInstancia();
    }

    /**
     *
     * @return
     */
    public static synchronized MediadorSeguridad getInstancia() {
        return instancia == null ? new MediadorSeguridad() : instancia;
    }

    /**
     *
     * @param usuario
     * @return
     */
    public PerfilDTO consultarDatosUsuarioLogueado(String usuario) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        PerfilDTO datosUsuario = null;
        ArrayList<MenuDTO> datosMenu = null;
        ArrayList<FuncionalidadDTO> datosFuncionalidades = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            datosUsuario = new PerfilDAO().consultarDatosUsuarioLogueado(conexion, usuario);

            if (datosUsuario != null) {
                datosMenu = new MenuDAO().listarMenusPorUsuario(conexion, datosUsuario.getIdTipoUsuario());

                if (datosMenu != null) {

                    datosUsuario.setMenu(datosMenu);
                    for (int i = 0; i < datosMenu.size(); i++) {

                        datosFuncionalidades = new FuncionalidadDAO().listarFuncionalidadesPorMenu(conexion, datosMenu.get(i).getId(), datosUsuario.getIdTipoUsuario());
                        if (datosFuncionalidades != null) {
                            datosMenu.get(i).setFuncionalidad(datosFuncionalidades);
                        }
                    }
                }
            }

        } catch (Exception e) {
            logMsg.loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                logMsg.loggerMessageException(e);
            }
        }
        return datosUsuario;
    }

    public PerfilDTO consultaUsuarioPorId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public PerfilDTO consultarUsuarioLogeado(String usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * LOS METODOS APARTIR DE AQUI NO HAN SIDO VALIDADOS
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     */
}
