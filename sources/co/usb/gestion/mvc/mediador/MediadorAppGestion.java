/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.mediador;

import co.usb.gestion.common.connection.ContextDataResourceNames;
import co.usb.gestion.common.connection.DataBaseConnection;
import co.usb.gestion.common.util.Generales;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dao.FormacionAcademicalDAO;
import co.usb.gestion.mvc.dao.NivelFormacionDAO;
import co.usb.gestion.mvc.dao.PerfilDAO;
import co.usb.gestion.mvc.dao.PerfilProductoDAO;
import co.usb.gestion.mvc.dao.SubTipoInvestigadorDAO;
import co.usb.gestion.mvc.dao.SubtipoProductoDAO;
import co.usb.gestion.mvc.dao.TipoInvestigadorDAO;
import co.usb.gestion.mvc.dao.TipoUsuarioDAO;
import co.usb.gestion.mvc.dao.UsuarioDAO;
import co.usb.gestion.mvc.dto.FormacionAcademicalDTO;
import co.usb.gestion.mvc.dto.NivelFormacionDTO;
import co.usb.gestion.mvc.dto.UsuarioDTO;
import co.usb.gestion.mvc.dto.PerfilDTO;
import co.usb.gestion.mvc.dto.PerfilProductoDTO;
import co.usb.gestion.mvc.dto.SubTipoInvestigadorDTO;
import co.usb.gestion.mvc.dto.SubtipoProductoDTO;
import co.usb.gestion.mvc.dto.TipoInvestigadorDTO;
import co.usb.gestion.mvc.dto.TipoUsuarioDTO;
import static java.lang.System.console;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.http.HttpSession;
import org.directwebremoting.WebContextFactory;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class MediadorAppGestion {

    /**
     *
     */
    private final static MediadorAppGestion instancia = null;
    private final LoggerMessage logMsg;

    /**
     *
     */
    public MediadorAppGestion() {
        logMsg = LoggerMessage.getInstancia();
    }

    /**
     *
     * @return
     */
    public static synchronized MediadorAppGestion getInstancia() {
        return instancia == null ? new MediadorAppGestion() : instancia;
    }

    public void pasarGarbageCollector() {
        Runtime garbage = Runtime.getRuntime();
        garbage.gc();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * LOS METODOS APARTIR DE AQUI NO HAN SIDO VALIDADOS
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     *
     */
    /**
     * @param idTipoInv
     * @return
     */
    public ArrayList<SubTipoInvestigadorDTO> listarSubTipoInvestigador(String idTipoInv) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new SubTipoInvestigadorDAO().listarSubTipoInvestigador(conexion, idTipoInv);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return listado;
    }

    /**
     *
     * @return
     */
    public ArrayList<TipoUsuarioDTO> listarTipoUsuario() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new TipoUsuarioDAO().listarTipoUsuario(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param documento
     * @return
     */
    public boolean validarDocumento(String documento) {
        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean usuarioValido = false;
        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            usuarioValido = new PerfilDAO().validarDocumento(conexion, documento);

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return usuarioValido;
    }

    /**
     *
     * @param datosUsuario
     * @param perfil
     * @return
     */
    public boolean registrarUsuario(UsuarioDTO datosUsuario, PerfilDTO perfil) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;

        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            conexion.setAutoCommit(false);

            if (!new UsuarioDAO().registrarUsuario(conexion, datosUsuario)) {
                throw new Exception("Error al registrar usuario.");
            }

            perfil.setIdUsuario(datosUsuario.getId());

            if (!new PerfilDAO().registrarPerfil(conexion, perfil)) {
                throw new Exception("Error al registrar perfil.");
            }
            registroExitoso = true;

        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (registroExitoso) {
                    conexion.commit();
                } else {
                    conexion.rollback();
                }
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param user
     * @return
     */
    public boolean validarUsuario(String user) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean validarUsuario = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            conexion.setAutoCommit(false);

            validarUsuario = new UsuarioDAO().validarUsuario(conexion, user);

            if (validarUsuario != false) {
                conexion.commit();
                validarUsuario = true;
            } else {
                conexion.rollback();
            }
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return validarUsuario;
    }

    /**
     * @param condicion
     * @return
     */
    public ArrayList<PerfilDTO> consultarPerfilPorNombreDocumento(String condicion) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilDAO().consultarPerfilPorNombreDocumento(conexion, condicion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarEstadoUsuario(UsuarioDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new UsuarioDAO().actualizarEstadoUsuario(conexion, datos)) {
                throw new Exception("Error al cambiar el estado.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param documento
     * @return
     */
    public UsuarioDTO recuperarContrasenia(String documento) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        UsuarioDTO datos = null;
        String nuevaContrasenia = Generales.EMPTYSTRING;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            datos = new PerfilDAO().recuperarContrasenia(conexion, documento);

            if (datos != null) {

                String usauId = datos.getId();
                nuevaContrasenia = UUID.randomUUID().toString().substring(0, 6);

                if (!new UsuarioDAO().generarContraseña(conexion, usauId, nuevaContrasenia)) {
                    throw new Exception("no se actualiza la coontrasaña");
                }
                datos.setClave(nuevaContrasenia);
            }

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return datos;
    }

    /**
     * @param condicion
     * @return
     */
    public ArrayList<PerfilDTO> consultarGestionPerfilProducto(String condicion) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<PerfilDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilDAO().consultarGestionPerfilProducto(conexion, condicion);

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @return
     */
    public ArrayList<TipoInvestigadorDTO> listarTipoInvestigador() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<TipoInvestigadorDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new TipoInvestigadorDAO().listarTipoInvestigador(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return listado;
    }

    /**
     * @param idPerfil
     * @return
     */
    public ArrayList<PerfilProductoDTO> ConsultarPerfilProducto(String idPerfil) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<PerfilProductoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilProductoDAO().ConsultarPerfilProducto(conexion, idPerfil);

            for (int i = 0; i < listado.size(); i++) {
                System.out.println("-------------- " + listado.get(i).toStringJson());
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return listado;
    }

    /**
     *
     * @return
     */
    public ArrayList<NivelFormacionDTO> listarNivelFormacion() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<NivelFormacionDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new NivelFormacionDAO().listarNivelFormacion(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param formacion
     * @return
     */
    public boolean registrarInfAcademica(FormacionAcademicalDTO formacion) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;

        boolean registroExitoso = true;

        try {
            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            formacion.setPerfilID(usuarioLogueado.getIdPerfil());
            if (!new FormacionAcademicalDAO().registrarFormacion(conexion, formacion)) {
                throw new Exception("Error al registrar formación academica.");

            }
            registroExitoso = true;

        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @param datos
     * @param user
     * @return
     */
    public boolean actualizarPerfil(PerfilDTO datos, UsuarioDTO user) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new PerfilDAO().actualizarPerfil(conexion, datos)) {
                throw new Exception("Error al cambiar el estado.");
            }

            if (!new UsuarioDAO().actualizarUsuario(conexion, user)) {
                throw new Exception("Error al cambiar el estado.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean actualizarFormacionAcad(FormacionAcademicalDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            datos.setPerfilID(usuarioLogueado.getIdPerfil());
            if (!new FormacionAcademicalDAO().actualizarFormacionAcad(conexion, datos)) {
                throw new Exception("Error al actualizar formación academica.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @param condicion
     * @return
     */
    public ArrayList<FormacionAcademicalDTO> consultarFormacionPorInstitucionPrograma(String condicion) {
        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new FormacionAcademicalDAO().consultarFormacionPorInstitucionPrograma(conexion, condicion, usuarioLogueado.getIdPerfil());

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @return
     */
    public ArrayList<PerfilDTO> listarUsuarios() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilDAO().listarUsuarios(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }
    
    /**
     *
     * @param Datos
     * @return
     */
    public boolean registrarPerfilProducto(PerfilProductoDTO Datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;

        boolean registroExitoso = true;

        try {
            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            Datos.setIdPerfil(usuarioLogueado.getIdPerfil());
            if (!new PerfilProductoDAO().registrarPerfilProducto(conexion, Datos)) {
                throw new Exception("Error al registrar perfil producto.");

            }
            registroExitoso = true;

        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }
    
     public boolean actualizarPerfilProducto(PerfilProductoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            datos.setIdPerfil(usuarioLogueado.getIdPerfil());
            if (!new PerfilProductoDAO().actualizarPerfilProducto(conexion, datos)) {
                throw new Exception("Error al actualizar perfil producto.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }
     
     /**
     * @param condicion
     * @return
     */
    public ArrayList<PerfilProductoDTO> consultarSubtipo(String condicion) {
        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            listado = new PerfilProductoDAO().consultarSubtipo(conexion, condicion, usuarioLogueado.getIdPerfil());
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }
    
    /**
     * 
     * @return
     */
    public ArrayList<SubtipoProductoDTO> listarSubtipoProducto() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<SubtipoProductoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new SubtipoProductoDAO().listarSubtipoProducto(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }
}
