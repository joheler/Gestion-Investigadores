<%-- 
    Document   : index
    Created on : 22/06/2018, 10:27:14 AM
    Author     : Ing Hebert Medelo
--%>

<%@page import="co.usb.gestion.mvc.dto.PerfilDTO"%>
<%@page import="co.usb.gestion.mvc.dto.MenuDTO"%>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<MenuDTO> menu = null;
    PerfilDTO datosUsuario = (PerfilDTO) session.getAttribute("datosUsuario");
    if (datosUsuario.getMenu() != null) {
        menu = datosUsuario.getMenu();
    }
%>

<!DOCTYPE html>
<html lang="en">
    <!-- Mirrored from www.g-axon.com/integral-2.0/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 26 Oct 2016 16:26:17 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Integral - A fully responsive, HTML5 based admin template">
        <meta name="keywords" content="Responsive, Web Application, HTML5, Admin Template, business, professional, Integral, web design, CSS3">
        <title>Gestion CV | USB</title>
        <!-- Site favicon -->
        <link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico' />
        <!-- /site favicon -->

        <!-- Entypo font stylesheet -->
        <link href="css/entypo.css" rel="stylesheet">
        <!-- /entypo font stylesheet -->

        <!-- Font awesome stylesheet -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <!-- /font awesome stylesheet -->

        <!-- Bootstrap stylesheet min version -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- /bootstrap stylesheet min version -->

        <!-- Integral core stylesheet -->
        <link href="css/integral-core.css" rel="stylesheet">
        <!-- /integral core stylesheet -->       

        <link href="css/integral-forms.css" rel="stylesheet">     
    </head>
    <body>
        <!-- Loader Backdrop -->
        <div class="loader-backdrop">           
            <!-- Loader -->
            <div class="loader">
                <div class="bounce-1"></div>
                <div class="bounce-2"></div>
            </div>
            <!-- /loader -->
        </div>
        <!-- loader backgrop -->

        <!-- Page container -->
        <div class="page-container">

            <!-- Page Sidebar -->
            <div class="page-sidebar">

                <!-- Site header  -->
                <header class="site-header">
                    <div class="site-logo"><a href="index.html"><img src="images/logo.png" alt="Integral" title="Integral"></a></div>
                    <div class="sidebar-collapse hidden-xs"><a class="sidebar-collapse-icon" href="#"><i class="icon-menu"></i></a></div>
                    <div class="sidebar-mobile-menu visible-xs"><a data-target="#side-nav" data-toggle="collapse" class="mobile-menu-icon" href="#"><i class="icon-menu"></i></a></div>
                </header>
                <!-- /site header -->

                <!-- Main navigation -->
                <ul id="side-nav" class="main-menu navbar-collapse collapse">
                    <li class="navigation-header">MEN�</li> 
                        <%for (int i = 0; i < menu.size(); i++) {%>
                    <li class="has-sub"><a href="collapsed-sidebar.html"><i class="<%=menu.get(i).getIconoMenu()%>"></i><span class="title"><%=menu.get(i).getTituloMenu()%></span></a>
                        <ul class="nav collapse">
                            <%for (int r = 0; r < menu.get(i).getFuncionalidad().size(); r++) {%>
                            <li class="" id="sudMenu<%=menu.get(i).getFuncionalidad().get(r).getId()%>" onclick="javascript:addActive(this.id);"><a href="<%=menu.get(i).getFuncionalidad().get(r).getPagina()%>"><span class="title"><%=menu.get(i).getFuncionalidad().get(r).getTitulo()%></span></a></li>
                                    <%}%>
                        </ul>
                    </li> 
                    <%}%>
                </ul>
                <!-- /main navigation -->		
            </div>
            <!-- /page sidebar -->

            <!-- Main container -->
            <div class="main-container">

                <!-- Main header -->
                <div class="main-header row">
                    <div class="col-sm-6 col-xs-7">

                        <!-- User info -->
                        <ul class="user-info pull-left">          
                            <li class="profile-info dropdown"><a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"> <img width="44" class="img-circle avatar" alt="" src="images/man-3.jpg"><%=datosUsuario.getNombre() + " " + datosUsuario.getApellido()%><span class="caret"></span></a>

                                <!-- User action menu -->
                                <ul class="dropdown-menu">
                                    <!--<li><a href="#/"><i class="icon-user"></i>My profile</a></li>
                                    <li><a href="#/"><i class="icon-mail"></i>Messages</a></li>
                                    <li><a href="#"><i class="icon-clipboard"></i>Tasks</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="icon-cog"></i>Account settings</a></li>-->
                                    <li><a href="login.jsp"><i class="icon-logout"></i>Logout</a></li>
                                </ul>
                                <!-- /user action menu -->
                            </li>
                        </ul>
                        <!-- /user info -->
                    </div>

                    <div class="col-sm-6 col-xs-5">
                        <div class="pull-right">
                            <!-- User alerts -->
                            <ul class="user-info pull-left">

                                <!-- Notifications -->
                                <li class="notifications dropdown">
                                    <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon-attention"></i><span class="badge badge-info">6</span></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li class="first">
                                            <div class="small"><a class="pull-right" href="#">Mark all Read</a> You have <strong>3</strong> new notifications.</div>
                                        </li>
                                        <li>
                                            <ul class="dropdown-list">
                                                <li class="unread notification-success"><a href="#"><i class="icon-user-add pull-right"></i><span class="block-line strong">New user registered</span><span class="block-line small">30 seconds ago</span></a></li>
                                                <li class="unread notification-secondary"><a href="#"><i class="icon-heart pull-right"></i><span class="block-line strong">Someone special liked this</span><span class="block-line small">60 seconds ago</span></a></li>
                                                <li class="unread notification-primary"><a href="#"><i class="icon-user pull-right"></i><span class="block-line strong">Privacy settings have been changed</span><span class="block-line small">2 hours ago</span></a></li>
                                                <li class="notification-danger"><a href="#"><i class="icon-cancel-circled pull-right"></i><span class="block-line strong">Someone special liked this</span><span class="block-line small">60 seconds ago</span></a></li>
                                                <li class="notification-info"><a href="#"><i class="icon-info pull-right"></i><span class="block-line strong">Someone special liked this</span><span class="block-line small">60 seconds ago</span></a></li>
                                                <li class="notification-warning"><a href="#"><i class="icon-rss pull-right"></i><span class="block-line strong">Someone special liked this</span><span class="block-line small">60 seconds ago</span></a></li>
                                            </ul>
                                        </li>
                                        <li class="external-last"> <a href="#">View all notifications</a> </li>
                                    </ul>
                                </li>
                                <!-- /notifications -->

                                <!-- Messages -->
                                <li class="notifications dropdown">
                                    <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon-mail"></i><span class="badge badge-secondary">12</span></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li class="first">
                                            <div class="dropdown-content-header"><i class="fa fa-pencil-square-o pull-right"></i> Messages</div>
                                        </li>
                                        <li>
                                            <ul class="media-list">
                                                <li class="media">
                                                    <div class="media-left"><img alt="" class="img-circle img-sm" src="images/domnic-brown.png"></div>
                                                    <div class="media-body">
                                                        <a class="media-heading" href="#">
                                                            <span class="text-semibold">Domnic Brown</span>
                                                            <span class="media-annotation pull-right">Tue</span>										
                                                        </a>
                                                        <span class="text-muted">Your product sounds interesting I would love to check this ne...</span>									
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <div class="media-left"><img alt="" class="img-circle img-sm" src="images/john-smith.png"></div>
                                                    <div class="media-body">
                                                        <a class="media-heading" href="#">
                                                            <span class="text-semibold">John Smith</span>
                                                            <span class="media-annotation pull-right">12:30</span>										
                                                        </a>
                                                        <span class="text-muted">Thank you for posting such a wonderful content. The writing was outstanding...</span>									
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <div class="media-left"><img alt="" class="img-circle img-sm" src="images/stella-johnson.png"></div>
                                                    <div class="media-body">
                                                        <a class="media-heading" href="#">
                                                            <span class="text-semibold">Stella Johnson</span>
                                                            <span class="media-annotation pull-right">2 days ago</span>										
                                                        </a>
                                                        <span class="text-muted">Thank you for trusting us to be your source for top quality sporting goods...</span>									
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <div class="media-left"><img alt="" class="img-circle img-sm" src="images/alex-dolgove.png"></div>
                                                    <div class="media-body">
                                                        <a class="media-heading" href="#">
                                                            <span class="text-semibold">Alex Dolgove</span>
                                                            <span class="media-annotation pull-right">10:45</span>										
                                                        </a>
                                                        <span class="text-muted">After our Friday meeting I was thinking about our business relationship and how fortunate...</span>									
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <div class="media-left"><img alt="" class="img-circle img-sm" src="images/domnic-brown.png"></div>
                                                    <div class="media-body">
                                                        <a class="media-heading" href="#">
                                                            <span class="text-semibold">Domnic Brown</span>
                                                            <span class="media-annotation pull-right">4:00</span>										
                                                        </a>
                                                        <span class="text-muted">I would like to take this opportunity to thank you for your cooperation in recently completing...</span>									
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="external-last"> <a  href="#">All Messages</a> </li>
                                    </ul>
                                </li>
                                <!-- /messages -->
                            </ul>
                            <!-- /user alerts -->
                        </div>
                    </div>
                </div>
                <!-- /main header -->

                <!-- Main content -->
                <div class="main-content" id="container">



                </div>
                <!-- /main content -->
            </div>
            <!-- /main container -->
        </div>
        <!-- /page container -->

        <!--Load JQuery-->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="plugins/metismenu/js/jquery.metisMenu.js"></script>
        <script src="plugins/blockui-master/js/jquery-ui.js"></script>
        <script src="plugins/blockui-master/js/jquery.blockUI.js"></script>  
        <script src="js/functions.js"></script>    
        <script src="js/jquery.validate.min.js"></script>

        <!--Functions Js-->        
        <script src="js/loader.js"></script>         

        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/interface/ajaxGestion.js"></script>
        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/interface/ajaxSeguridad.js"></script>
        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/engine.js"></script>
        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/util.js"></script>


        <script>
                                function addActive(id) {
                                    $("#side-nav li ul li").each(function () {
                                        $(this).removeClass("active");
                                    });
                                    $("#" + id).addClass("active");
                                }

                                function cargarPagina(pagina) {
                                    $('#container').load(pagina);
                                }
                                
                                function validar(r, i) {
                                   operacion = i;
                                   jQuery('#' + r).validate({
                                       highlight: function (label) {
                                           jQuery(label).closest('.form-group').removeClass('has-success').addClass('has-error');
                                       },
                                       success: function (label) {
                                           jQuery(label).closest('.form-group').removeClass('has-error');
                                           label.remove();
                                       },
                                       errorPlacement: function (error, element) {
                                           var placement = element.closest('.input-group');
                                           if (!placement.get(0)) {
                                               placement = element;
                                           }
                                           if (error.text() !== '') {
                                               placement.after(error);
                                           }
                                       },
                                       submitHandler: function () {
                                           ejecutarPostValidate();
                                       }
                                   });
                               }

        </script>   
    </body>

    <!-- Mirrored from www.g-axon.com/integral-2.0/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 26 Oct 2016 16:27:05 GMT -->
</html>

