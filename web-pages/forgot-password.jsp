﻿<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from www.g-axon.com/integral-2.0/light/forgot-password.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 26 Oct 2016 16:28:39 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Integral - A fully responsive, HTML5 based admin template">
        <meta name="keywords" content="Responsive, Web Application, HTML5, Admin Template, business, professional, Integral, web design, CSS3">
        <title>Integral | Forgot Password</title>
        <!-- Site favicon -->
        <link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico' />
        <!-- /site favicon -->

        <!-- Entypo font stylesheet -->
        <link href="css/entypo.css" rel="stylesheet">
        <!-- /entypo font stylesheet -->

        <!-- Font awesome stylesheet -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <!-- /font awesome stylesheet -->

        <!-- Bootstrap stylesheet min version -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- /bootstrap stylesheet min version -->

        <!-- Integral core stylesheet -->
        <link href="css/integral-core.css" rel="stylesheet">
        <!-- /integral core stylesheet -->

        <link href="css/integral-forms.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
              <script src="js/html5shiv.min.js"></script>
              <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="login-page">

        <!-- Loader Backdrop -->
        <div class="loader-backdrop">           
            <!-- Loader -->
            <div class="loader">
                <div class="bounce-1"></div>
                <div class="bounce-2"></div>
            </div>
            <!-- /loader -->
        </div>
        <!-- loader backgrop -->

        <div class="login-container">
            <div class="login-branding">
                <a href="index.html"><img src="images/logo.png" alt="Integral" title="Integral"></a>
            </div>
            <div class="login-content">
                <div id="alert"></div>
                <h2>¿Olvidaste tu contraseña?</h2>
                <p>No se preocupe, le enviaremos un correo electrónico para restablecer su contraseña.</p>
                <form role="form" action="return:false" id="contactForm">                        
                    <div class="form-group">
                        <input maxlength="10" type="text" id="documento" placeholder="Ingrese su documento" class="form-control" title="Ingrese su documento" required>
                    </div>  
                    <p>¿No has podido recoperar tu contraseña? <a href="#">Contacta a soporte</a>.</p>                      
                    <div class="form-group">
                        <button type="submit" id="dtnContinuar" class="btn btn-primary btn-block" onclick="javascript:validar();">Recuperar contraña</button>
                    </div>
                </form>
            </div>
        </div>
        <!--Load JQuery-->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/loader.js"></script>        
        <script src="js/jquery.validate.min.js"></script>
    </body>
    <script>
                            $(document).ready(function () {
                                $("#alertCorrecto").hide();
                                $("#alertError").hide();
                            });


                            function validar() {

                                $('#contactForm').validate({
                                    highlight: function (label) {
                                        jQuery(label).closest('.form-group').removeClass('has-success').addClass('has-error');
                                    },
                                    success: function (label) {
                                        jQuery(label).closest('.form-group').removeClass('has-error');
                                        label.remove();
                                    },
                                    errorPlacement: function (error, element) {
                                        var placement = element.closest('.input-group');
                                        if (!placement.get(0)) {
                                            placement = element;
                                        }
                                        if (error.text() !== '') {
                                            placement.after(error);
                                        }
                                    },
                                    submitHandler: function () {
                                        llamarServlet();
                                    }
                                });
                            }


                            function llamarServlet() {
                                $('#dtnContinuar').attr("disabled", true);
                                $.ajax({
                                    url: 'ServletRecuperarClave',
                                    data: {
                                        documento: $("#documento").val()
                                    },
                                    success: function (data) {
                                        notificacion("alert-success", data);
                                        $('#dtnContinuar').attr("disabled", false);
                                    }
                                });
                            }

                            function notificacion(t, m) {                              
                                $("#alert").text("");
                                $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                        + '<span aria-hidden="true">×</span>'
                                        + '</button>'
                                        + m
                                        + '</div>');
                                $("#alert").focus();

                            }

    </script>

</html>
