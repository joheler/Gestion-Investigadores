<%-- 
    Document   : listar-usuario
    Created on : 1/07/2018, 04:23:55 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link href="plugins/datatables/css/jquery.dataTables.css" rel="stylesheet">
<link href="plugins/datatables/extensions/Buttons/css/buttons.dataTables.css" rel="stylesheet">
<h1 class="page-title">Listar Investigador</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="#">Usuario</a></li> 
    <li class="active"><strong>Listar</strong></li> 
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Listado</h4>
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div id="alert"></div>                
                <div class="table-responsive" id="divTabla">
                    <table class="table table-striped table-bordered table-hover dataTables-example" id="myTable">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Documento</th>
                                <th>Teléfono</th>
                                <th>Correo</th>
                                <th>T. usuario</th>
                                <th>ST. investigador</th>
                                <th>Esatado</th>
                            </tr>
                        </thead>
                        <tbody id="lisUsuario"></tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th>Documento</th>
                                <th>Teléfono</th>
                                <th>Correo</th>
                                <th>T. usuario</th>
                                <th>ST. investigador</th>
                                <th>Esatado</th>                                
                            </tr>
                        </tfoot>
                    </table>
                </div>               
            </div>
        </div>
    </div>
</div>

<script src="plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables/js/jszip.min.js"></script>
<script src="plugins/datatables/js/pdfmake.min.js"></script>
<script src="plugins/datatables/js/vfs_fonts.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
    $(document).ready(function () {
        listarUsuarios();
    });
    function listarUsuarios() {
        ajaxGestion.listarUsuarios({
            callback: function (data) {
                console.log(data);
                if (data !== null) {
                    listado = data;
                    $('#myTable').dataTable().fnDestroy();
                    dwr.util.removeAllRows("lisUsuario");
                    dwr.util.addRows("lisUsuario", listado, mapa, {
                        escapeHtml: false
                    });

                    $(".table-responsive").show();
                    $("#formRegistrar").hide();
                    $('.dataTables-example').DataTable({
                        dom: '<"html5buttons" B>lTfgitp',
                        buttons: [
                            {
                                extend: 'copyHtml5',
                                exportOptions: {
                                    columns: [0, ':visible']
                                }
                            },
                            {
                                extend: 'excelHtml5',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            {
                                extend: 'pdfHtml5',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4]
                                }
                            },
                            'colvis'
                        ]
                    });
                } else {
                    notificacion("alert-danger", "No se <strong>encontraron</strong> resultados.");
                }
            },
            timeout: 20000
        });
    }
    var listado = [];
    var mapa = [
        function (data) {
            return data.nombre + " " + data.apellido;
        },
        function (data) {
            return data.documento;
        },
        function (data) {
            return data.telefono;
        },
        function (data) {
            return data.correo;
        },
        function (data) {
            return data.tipoUsuario;
        },
        function (data) {
            return data.idSubtipoInvesstigador;
        },
        function (data) {
            if (data.estado == "1") {
                return "Activo";
            } else {
                return "Inactivo";
            }
        }
    ];
</script>