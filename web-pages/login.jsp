<%-- 
    Document   : login
    Created on : 22/06/2018, 10:32:55 AM
    Author     : Hebert Medelo
--%>

<%@page import="co.usb.gestion.common.util.ValidaString"%>
<%@ page import="javax.servlet.*"%>
<%@ page import="java.util.*,java.io.*"  contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%

    session.invalidate();

    String mensajeError = request.getParameter("error");

    boolean mostrarError = false;

    if (!ValidaString.isNullOrEmptyString(mensajeError)) {
        mostrarError = true;
    }
%>

<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from www.g-axon.com/integral-2.0/light/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 26 Oct 2016 16:28:39 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Integral - A fully responsive, HTML5 based admin template">
        <meta name="keywords" content="Responsive, Web Application, HTML5, Admin Template, business, professional, Integral, web design, CSS3">
        <title>Integral | Login</title>
        <!-- Site favicon -->
        <link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico' />
        <!-- /site favicon -->

        <!-- Entypo font stylesheet -->
        <link href="css/entypo.css" rel="stylesheet">
        <!-- /entypo font stylesheet -->

        <!-- Font awesome stylesheet -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <!-- /font awesome stylesheet -->

        <!-- Bootstrap stylesheet min version -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- /bootstrap stylesheet min version -->

        <!-- Integral core stylesheet -->
        <link href="css/integral-core.css" rel="stylesheet">
        <!-- /integral core stylesheet -->

        <link href="css/integral-forms.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
              <script src="js/html5shiv.min.js"></script>
              <script src="js/respond.min.js"></script>
        <![endif]-->


    </head>
    <body class="login-page">

        <!-- Loader Backdrop -->
        <div class="loader-backdrop">           
            <!-- Loader -->
            <div class="loader">
                <div class="bounce-1"></div>
                <div class="bounce-2"></div>
            </div>
            <!-- /loader -->
        </div>
        <!-- loader backgrop -->

        <div class="login-container">
            <div class="login-branding">
                <a href="index.html"><img src="images/logo.png" alt="Integral" title="Integral"></a>
            </div>
            <div class="login-content">
                <h2><strong>Welcome</strong>, please login</h2>
                <form role="form" action="<%= request.getContextPath()%>/j_security_check" method="post" autocomplete="off">  


                    <%if (mostrarError) {%>                           
                    <p>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><%= mensajeError%></strong>
                    </div>
                    </p>
                    <% }%>

                    <div class="form-group">
                        <input type="text" name="j_username" placeholder="User" required="" class="form-control">
                    </div>                        
                    <div class="form-group">
                        <input type="password" name="j_password" placeholder="Password" required="" class="form-control">
                    </div>



                    <div class="form-group">
                        <div class="checkbox checkbox-replace">
                            <input type="checkbox" id="remeber">
                            <label for="remeber">Remeber me</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" value="Ingresar" class="btn btn-primary btn-block">Login</button>
                    </div>
                    <p class="text-center"><a href="forgot-password.jsp">Forgot your password?</a></p>                        
                </form>
            </div>
        </div>

        <!--Load JQuery-->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/loader.js"></script>

    </body>

    <!-- Mirrored from www.g-axon.com/integral-2.0/light/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 26 Oct 2016 16:28:39 GMT -->
</html>
