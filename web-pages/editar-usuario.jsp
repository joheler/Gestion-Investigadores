<%-- 
    Document   : editar-usuario
    Created on : 1/07/2018, 04:23:55 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link href="plugins/datatables/css/jquery.dataTables.css" rel="stylesheet">
<link href="plugins/datatables/extensions/Buttons/css/buttons.dataTables.css" rel="stylesheet">
<h1 class="page-title">Editar Investigador</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="#">Usuario</a></li> 
    <li class="active"><strong>Editar</strong></li> 
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Editar</h4>
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div id="alert"></div>
                <form class="form-horizontal" role="form" id="formRegistrar" action="return:false" autocomplete="off">
                    <div class="form-group"><label class="col-sm-3 control-label">Ingrese nombre/documento</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-md-7">
                                    <input type="text" id="buscar" class="form-control" placeholder="Nombre/Documento" required title="Ingrese nombre o documento">
                                </div>
                                <div class="col-md-5">
                                    <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formRegistrar', 1);">Buscar</button>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </form>
                <div class="table-responsive" hidden id="divTabla">
                    <table class="table table-striped table-bordered table-hover dataTables-example" id="myTable">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Documento</th>
                                <th>Correo</th>
                                <th>T. usuario</th>
                                <th>Esatado</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody id="lisUsuario"></tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th>Documento</th>
                                <th>Correo</th>
                                <th>T. usuario</th>
                                <th>Esatado</th>
                                <th>Editar</th>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="button" class="btn btn-white btn-rounded" onclick="volver();">volver</button>                       
                        </div>
                    </div>
                </div>
                <form role="form" id="formEditar" action="return:false" autocomplete="off" hidden>
                    <div id="alert"></div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required maxlength="50" title="Ingrese nombre">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="apellido">Apellido</label>
                            <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" required maxlength="50" title="Ingrese apellido">
                        </div>                     
                    </div> 
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="tUsuario">Tipo de usuario</label>
                            <select onchange="javascript:verTipoInvestigador(this.value);" class="form-control" id="tUsuario" name="tUsuario" required title="Seleccione una opcion"></select>
                        </div>
                        <div class="form-group col-lg-3" hidden id="divTinvestigador">                                                   
                            <label class="control-label" for="tInvestigador">Tipo de investigador</label>
                            <select onchange="javascript:listarSubTipoInvestigador(this.value, 'sInvestigador');" class="form-control" id="tInvestigador" name="tInvestigador" title="Seleccione una opcion"></select>
                        </div>
                        <div class="form-group col-lg-3" hidden id="divSinvestigador">                                                   
                            <label class="control-label" for="sInvestigador">Sudtipo de investigador</label>
                            <select class="form-control" id="sInvestigador" name="sInvestigador" title="Seleccione una opcion"></select>
                        </div>                         
                    </div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="documento">No. documento</label>
                            <input remote="ValidarDocumento" type="tel" class="form-control" id="documento" name="documento" placeholder="No. documento" required disabled maxlength="10" title="Ingrese un documento que no este registrado">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="direccion">Dirección</label>
                            <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" required maxlength="100" title="Ingrese dirección">
                        </div>                     
                    </div> 
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="telefono">Teléfono</label>
                            <input type="tel" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" required maxlength="10" title="Ingrese teléfono">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="correo">Correo</label>
                            <input remote="validarUsuario" type="email" class="form-control" id="correo" name="correo" placeholder="Correo" required disabled maxlength="50" title="Ingrese un correo que no este resgistrado" onkeyup="javascript:$('#usuario').val(this.value)">
                        </div>                     
                    </div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="sexo">Sexo</label>
                            <select class="form-control" id="sexo" name="sexo" required title="Seleccione una opcion">
                                <option value="">Seleccione una opción</option>
                                <option value="1">Hombre</option>
                                <option value="0">Mujer</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="estado">Estado</label>
                            <select class="form-control" id="estado" name="estado" required title="Seleccione una opcion">
                                <option value="">Seleccione una opción</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div> 
                    </div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="usuario">Usuario</label>
                            <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuario" disabled required maxlength="50" title="Ingrese usuario">
                        </div>
                        <div class="form-group col-lg-6">                 
                        </div>                     
                    </div>
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="reset" class="btn btn-white btn-rounded" onclick="volver2()">Volver</button>
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formEditar', 2);">Editar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables/js/jszip.min.js"></script>
<script src="plugins/datatables/js/pdfmake.min.js"></script>
<script src="plugins/datatables/js/vfs_fonts.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
                                var idSubtipoInvesstigador = null;
                                var idPerfil;
                                var idUsuario;
                                $(document).ready(function () {
                                    listarTipoUsuario("tUsuario");
                                    listarTipoInvestigador("tInvestigador");
                                });
                                function cambiarEstado(id) {
                                    for (var i = 0; i < listado.length; i++) {
                                        if (listado[i].id == id) {
                                            if (listado[i].estado == "1") {
                                                listado[i].estado = "0";
                                            } else if (listado[i].estado == "0") {
                                                listado[i].estado = "1";
                                            }
                                            var datos = {
                                                id: listado[i].idUsuario,
                                                estado: listado[i].estado
                                            };
                                        }
                                    }
                                    ajaxGestion.actualizarEstadoUsuario(datos, {
                                        callback: function (data) {
                                            if (data) {
                                                $('#myTable').dataTable().fnDestroy();
                                                dwr.util.removeAllRows("lisUsuario");
                                                dwr.util.addRows("lisUsuario", listado, mapa, {
                                                    escapeHtml: false
                                                });
                                                $('.dataTables-example').DataTable({
                                                    dom: '<"html5buttons" B>lTfgitp',
                                                    buttons: [
                                                        {
                                                            extend: 'copyHtml5',
                                                            exportOptions: {
                                                                columns: [0, ':visible']
                                                            }
                                                        },
                                                        {
                                                            extend: 'excelHtml5',
                                                            exportOptions: {
                                                                columns: ':visible'
                                                            }
                                                        },
                                                        {
                                                            extend: 'pdfHtml5',
                                                            exportOptions: {
                                                                columns: [0, 1, 2, 3, 4]
                                                            }
                                                        },
                                                        'colvis'
                                                    ]
                                                });
                                                notificacion("alert-success", "se ha <strong>actualizado</strong> el estado con exíto.");
                                            } else {
                                                notificacion("alert-danger", "Error al <strong>actualizar</strong> el estado.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }
                                function volver() {
                                    $(".table-responsive").hide();
                                    $("#formRegistrar").show();
                                    $("#buscar").val("");
                                }
                                function volver2() {
                                    $("#formEditar").hide();
                                    $("#divTabla").show();
                                }
                                function consultarPerfilPorNombreDocumento() {
                                    var condicion = $("#buscar").val();
                                    ajaxGestion.consultarPerfilPorNombreDocumento(condicion, {
                                        callback: function (data) {
                                            console.log(data);
                                            if (data !== null) {
                                                listado = data;
                                                $('#myTable').dataTable().fnDestroy();
                                                dwr.util.removeAllRows("lisUsuario");
                                                dwr.util.addRows("lisUsuario", listado, mapa, {
                                                    escapeHtml: false
                                                });

                                                $(".table-responsive").show();
                                                $("#formRegistrar").hide();
                                                $('.dataTables-example').DataTable({
                                                    dom: '<"html5buttons" B>lTfgitp',
                                                    buttons: [
                                                        {
                                                            extend: 'copyHtml5',
                                                            exportOptions: {
                                                                columns: [0, ':visible']
                                                            }
                                                        },
                                                        {
                                                            extend: 'excelHtml5',
                                                            exportOptions: {
                                                                columns: ':visible'
                                                            }
                                                        },
                                                        {
                                                            extend: 'pdfHtml5',
                                                            exportOptions: {
                                                                columns: [0, 1, 2, 3, 4]
                                                            }
                                                        },
                                                        'colvis'
                                                    ]
                                                });
                                            } else {
                                                notificacion("alert-danger", "No se <strong>encontraron</strong> resultados.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }
                                var listado = [];
                                var mapa = [
                                    function (data) {
                                        return data.nombre + " " + data.apellido;
                                    },
                                    function (data) {
                                        return data.documento;
                                    },
                                    function (data) {
                                        return data.correo;
                                    },
                                    function (data) {
                                        return data.tipoUsuario;
                                    },
                                    function (data) {
                                        if (data.estado == "1") {
                                            return "<button class='btn btn-default btn-rounded btn-xs' type='button' onclick='cambiarEstado(" + data.id + ");'><i class='icon-check icon-larger green-color'></i></button>";
                                        } else {
                                            return "<button class='btn btn-default btn-rounded btn-xs' type='button' onclick='cambiarEstado(" + data.id + ");'><i class='icon-cancel icon-larger red-color'></i></button>";
                                        }
                                    },
                                    function (data) {
                                        return "<button class='btn btn-primary btn-rounded btn-xs' type='button'  onclick='verCliente(" + data.id + ");'>Ver</button>";
                                    }
                                ];
                                function ejecutarPostValidate() {
                                    if (operacion == 1)
                                        consultarPerfilPorNombreDocumento();
                                    if (operacion == 2)
                                        editarUsuario();
                                    operacion = null;
                                }
                                function notificacion(t, m) {
                                    if ($('#alert').text() == "") {
                                        setTimeout('$("#alert").text("")', 3000);
                                    }
                                    $("#alert").text("");
                                    $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                                            + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                            + '<span aria-hidden="true">×</span>'
                                            + '</button>'
                                            + m
                                            + '</div>');
                                    $("#alert").focus();

                                }
                                function verCliente(id) {
                                    for (var i = 0; i < listado.length; i++) {
                                        if (listado[i].id == id) {
                                            console.log(listado[i]);
                                            idPerfil = id;
                                            idUsuario = listado[i].idUsuario;
                                            $("#nombre").val(listado[i].nombre);
                                            $("#apellido").val(listado[i].apellido);
                                            $("#documento").val(listado[i].documento);
                                            $("#direccion").val(listado[i].direccion);
                                            $("#telefono").val(listado[i].telefono);
                                            $("#correo").val(listado[i].correo);
                                            $("#sexo").val(listado[i].sexo);
                                            $("#estado").val(listado[i].estado);
                                            $("#usuario").val(listado[i].usuario);
                                            $("#tUsuario").val(listado[i].idTipoUsuario).trigger("change");
                                            $("#tInvestigador").val(listado[i].idTipoInvestigador).trigger("change");
                                            idSubtipoInvesstigador = listado[i].idSubtipoInvesstigador;
                                        }
                                    }
                                    $("#divTabla").hide();
                                    $("#formEditar").show();
                                }
                                function listarTipoInvestigador(idCombo, valorSeleccionado) {
                                    ajaxGestion.listarTipoInvestigador({
                                        callback: function (data) {
                                            if (data !== null) {
                                                dwr.util.removeAllOptions(idCombo);
                                                dwr.util.addOptions(idCombo, [{
                                                        id: '',
                                                        descripcion: 'Seleccione una opción'
                                                    }], 'id', 'descripcion');
                                                dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                                                $("#" + idCombo).val(valorSeleccionado).trigger("change");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }
                                function listarTipoUsuario(idCombo) {
                                    ajaxGestion.listarTipoUsuario({
                                        callback: function (data) {
                                            if (data !== null) {
                                                dwr.util.removeAllOptions(idCombo);
                                                dwr.util.addOptions(idCombo, [{
                                                        id: '',
                                                        descripcion: 'Seleccione una opción'
                                                    }], 'id', 'descripcion');
                                                dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                                                //jQuery("#" + idCombo).val(valorSeleccionado);
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }
                                function listarSubTipoInvestigador(idTipoInv, idCombo) {
                                    ajaxGestion.listarSubTipoInvestigador(idTipoInv, {
                                        callback: function (data) {
                                            dwr.util.removeAllOptions(idCombo);
                                            dwr.util.addOptions(idCombo, [{
                                                    id: '',
                                                    descripcion: 'Seleccione una opción'
                                                }], 'id', 'descripcion');
                                            if (data !== null) {
                                                dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                                                if (idSubtipoInvesstigador != null) {
                                                    jQuery("#" + idCombo).val(idSubtipoInvesstigador);
                                                }
                                            }
                                            idSubtipoInvesstigador = null;
                                        },
                                        timeout: 20000
                                    });
                                }
                                function verTipoInvestigador(id) {
                                    if (id == "2") {
                                        $("#divSinvestigador").show();
                                        $("#divTinvestigador").show();
                                        $('#sInvestigador').prop("required", true);
                                        $('#tInvestigador').prop("required", true);
                                    } else {
                                        $("#divSinvestigador").hide();
                                        $("#divTinvestigador").hide();
                                        $('#sInvestigador').prop("required", false);
                                        $('#tInvestigador').prop("required", false);
                                        $('#sInvestigador').val("");
                                        $('#tInvestigador').val("");
                                    }
                                }
                                function editarUsuario() {
                                    var usuario = {
                                        id: idUsuario,
                                        idTipoUsuario: $("#tUsuario").val(),
                                        estado: $("#estado").val()
                                    };
                                    var perfil = {
                                        id: idPerfil,
                                        nombre: $("#nombre").val(),
                                        apellido: $("#apellido").val(),
                                        idSubtipoInvesstigador: $("#sInvestigador").val(),
                                        direccion: $("#direccion").val(),
                                        telefono: $("#telefono").val(),
                                        sexo: $("#sexo").val()
                                    };
                                    ajaxGestion.actualizarPerfil(perfil, usuario, {
                                        callback: function (data) {
                                            console.log(data);
                                            if (data) {
                                                notificacion("alert-success", "Se <strong>registraron</strong> los datos on exíto.");
                                                $("#formRegistrar")[0].reset();
                                            } else {
                                                notificacion("alert-danger", "Error al <strong>registrar</strong> los datos.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }

</script>