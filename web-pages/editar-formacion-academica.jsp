<%-- 
    Document   : formacion-academica
    Created on : 1/07/2018, 04:23:55 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link href="plugins/datatables/css/jquery.dataTables.css" rel="stylesheet">
<link href="plugins/datatables/extensions/Buttons/css/buttons.dataTables.css" rel="stylesheet">
<h1 class="page-title">Formación Academica</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="#">Investigador</a></li> 
    <li class="active"><strong>Editar Formación Academica</strong></li> 
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a id="cargar" data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div id="alert"></div>
                <form class="form-horizontal" role="form" id="formBuscar" action="return:false" autocomplete="off">
                    <div class="form-group"><label class="col-sm-3 control-label">Ingrese Institución/Programa</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-md-7">
                                    <input type="text" id="buscar" class="form-control" placeholder="Institucion/Programa" required title="Ingrese Insitución o Programa">
                                </div>
                                <div class="col-md-5">
                                    <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formBuscar', 1);">Buscar</button>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </form>
                <div class="table-responsive" hidden id="divTabla">
                    <table class="table table-striped table-bordered table-hover dataTables-example" id="myTable">
                        <thead>
                            <tr>
                                <th>Fecha inicio</th>
                                <th>Graduación</th>
                                <th>Nivel Formación</th>
                                <th>Institución</th>
                                <th>Programa</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody id="lisformacion"></tbody>
                        <tfoot>
                            <tr>
                                <th>Fecha inicio</th>
                                <th>Graduación</th>
                                <th>Nivel Formación</th>
                                <th>Institución</th>
                                <th>Programa</th>
                                <th>Editar</th>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="button" class="btn btn-white btn-rounded" onclick="volver();">volver</button>                       
                        </div>
                    </div>
                </div>
                <form role="form" id="formEditar" action="return:false" autocomplete="off" hidden>
                    <div id="alert"></div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="fecha">Fecha de inicio</label>
                            <input type="text" class="form-control" id="fecha" name="fecha" placeholder="Año de inicio" required maxlength="50" title="Ingrese año de inicio">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="graduacion">Año de graduación</label>
                            <input type="text" class="form-control" id="graduacion" name="graduacion" placeholder="Año de graduación" required maxlength="50" title="Ingrese año de graduación">
                        </div>                     
                    </div> 
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="nivel">Nivel de formación</label>
                            <select class="form-control" id="nivel" name="nivel" required title="Seleccione una opcion"></select>
                        </div>                        
                    </div>
                    <div class="row"> 
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="institucion">Institución</label>
                            <input type="text" class="form-control" id="institucion" name="institucion" placeholder="Institución" required maxlength="100" title="Ingrese la Institución">
                        </div>   
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="programa">Programa academico</label>
                            <input type="text" class="form-control" id="programa" name="programa" placeholder="Programa academico" required maxlength="100" title="Ingrese el programa academico">
                        </div>                  
                    </div> 
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="reset" class="btn btn-white btn-rounded" onclick="volver2()">Volver</button>
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formEditar', 2);">Editar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables/js/jszip.min.js"></script>
<script src="plugins/datatables/js/pdfmake.min.js"></script>
<script src="plugins/datatables/js/vfs_fonts.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
                                var operacion = null;
                                var IdFormacion = null;
                                $(document).ready(function () {
                                    listarNivelFormacion("nivel");
                                    $('#cargar').click(function(){ consultarFormacionPorInstitucionPrograma(); return false; });
                                });
                                function volver() {
                                    $(".table-responsive").hide();
                                    $("#formEditar").show();
                                    $("#formBuscar").val("");
                                }
                                function volver2() {
                                    $("#formEditar").hide();
                                    $("#divTabla").show();
                                }
                                function actualizarInfAcademica() {
                                    
                                    var Datos = {
                                        id: IdFormacion,
                                        fecha: $("#fecha").val(),
                                        graduacion: $("#graduacion").val(),
                                        nivel: $("#nivel").val(),
                                        institucion: $("#institucion").val(),
                                        programa: $("#programa").val()
                                    };
                                    ajaxGestion.actualizarFormacionAcad(Datos, {
                                        callback: function (data) {
                                            if (data) {
                                                notificacion("alert-success", "Se <strong>actualizaron</strong> los datos on exíto.");
                                                $("#formBuscar")[0].reset();
                                            } else {
                                                notificacion("alert-danger", "Error al <strong>actualizar</strong> los datos.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }

                                function ejecutarPostValidate() {
                                    if (operacion == 1)
                                        consultarFormacionPorInstitucionPrograma();
                                    if (operacion == 2)
                                        actualizarInfAcademica();
                                    // if (operacion == 2)
                                    operacion = null;
                                    IdFormacion = null;
                                }
                                function consultarFormacionPorInstitucionPrograma() {
                                    var condicion = $("#formBuscar").val();
                                    ajaxGestion.consultarFormacionPorInstitucionPrograma(condicion, {
                                        callback: function (data) {
                                            if (data !== null) {
                                                listado = data;
                                                $('#myTable').dataTable().fnDestroy();
                                                dwr.util.removeAllRows("lisformacion");
                                                dwr.util.addRows("lisformacion", listado, mapa, {
                                                    escapeHtml: false
                                                });

                                                $(".table-responsive").show();
                                                $("#formEditar").hide();
                                                $('.dataTables-example').DataTable({
                                                    dom: '<"html5buttons" B>lTfgitp',
                                                    buttons: [
                                                        {
                                                            extend: 'copyHtml5',
                                                            exportOptions: {
                                                                columns: [0, ':visible']
                                                            }
                                                        },
                                                        {
                                                            extend: 'excelHtml5',
                                                            exportOptions: {
                                                                columns: ':visible'
                                                            }
                                                        },
                                                        {
                                                            extend: 'pdfHtml5',
                                                            exportOptions: {
                                                                columns: [0, 1, 2, 3, 4]
                                                            }
                                                        },
                                                        'colvis'
                                                    ]
                                                });
                                            } else {
                                                notificacion("alert-danger", "No se <strong>encontraron</strong> resultados.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }
                                var listado = [];
                                var mapa = [
                                    function (data) {
                                        return data.fecha;
                                    },
                                    function (data) {
                                        return data.graduacion;
                                    },
                                    function (data) {
                                        return data.nivel;
                                    },
                                    function (data) {
                                        return data.institucion;
                                    },
                                    function (data) {
                                        return data.programa;
                                    },
                                    function (data) {
                                        return "<button class='btn btn-primary btn-rounded btn-xs' type='button'  onclick='verFormacion(" + data.id + ");'>Ver</button>";
                                    }
                                ];
                                function notificacion(t, m) {
                                    if ($('#alert').text() == "") {
                                        setTimeout('$("#alert").text("")', 3000);
                                    }
                                    $("#alert").text("");
                                    $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                                            + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                            + '<span aria-hidden="true">×</span>'
                                            + '</button>'
                                            + m
                                            + '</div>');
                                    $("#alert").focus();

                                }
                                function verFormacion(id) {
                                    for (var i = 0; i < listado.length; i++) {
                                        if (listado[i].id == id) {
                                            $("#fecha").val(listado[i].fecha);
                                            $("#graduacion").val(listado[i].graduacion);
                                            $('#nivel option')
                                                    .filter(function () {
                                                        return $.trim($(this).text()) == listado[i].nivel;
                                                    })
                                                    .attr('selected', true);

                                            $("#institucion").val(listado[i].institucion);
                                            $("#programa").val(listado[i].programa);

                                            IdFormacion = listado[i].id;
                                        }
                                    }



                                    $("#divTabla").hide();
                                    $("#formEditar").show();
                                }
                                function listarNivelFormacion(idCombo) {
                                    ajaxGestion.listarNivelFormacion({
                                        callback: function (data) {
                                            if (data !== null) {
                                                dwr.util.removeAllOptions(idCombo);
                                                dwr.util.addOptions(idCombo, [{
                                                        id: '',
                                                        descripcion: 'Seleccione una opción'
                                                    }], 'id', 'descripcion');
                                                dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }

</script>