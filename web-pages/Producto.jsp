<%-- 
    Document   : Producto
    Created on : 1/07/2018, 04:23:55 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link href="plugins/datatables/css/jquery.dataTables.css" rel="stylesheet">
<link href="plugins/datatables/extensions/Buttons/css/buttons.dataTables.css" rel="stylesheet">
<h1 class="page-title">Perfil Producto</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="#">Investigador</a></li> 
    <li class="active"><strong>Perfil Producto</strong></li> 
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <form role="form" id="formRegistrar" action="return:false" autocomplete="off">
                    <div id="alert"></div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required maxlength="50" title="Ingrese nombre">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="descripcion">Descripción</label>
                            <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="descripción" required maxlength="50" title="Ingrese la descripción">
                        </div>                     
                    </div> 
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="subtipo">Subtipo de producto</label>
                            <select class="form-control" id="subtipo" name="subtipo" required title="Seleccione una opcion"></select>
                        </div>                                                                   
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="fecha">Fecha</label>
                            <input type="text" class="form-control" id="fecha" name="fecha" placeholder="Fecha" required maxlength="50" title="Ingrese fecha">
                        </div>
                    </div>                    
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="reset" class="btn btn-white btn-rounded">Cancelar</button>
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formRegistrar',2);">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables/js/jszip.min.js"></script>
<script src="plugins/datatables/js/pdfmake.min.js"></script>
<script src="plugins/datatables/js/vfs_fonts.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
                                var operacion = null;
                                $(document).ready(function () {
                                    listarSubtipoProducto("subtipo");
                                });
                                function volver() {
                                    $(".table-responsive").hide();
                                    $("#formRegistrar").show();
                                }
                                function registrarPerfilProducto() {
                                    
                                    var Datos = {
                                        fecha: $("#fecha").val(),
                                        nombre: $("#nombre").val(),
                                        descripcion: $("#descripcion").val(),
                                        suprid: $("#subtipo").val()
                                    };
                                    
                                    
                                    ajaxGestion.registrarPerfilProducto(Datos, {
                                        callback: function (data) {
                                            if (data) {
                                                notificacion("alert-success", "Se <strong>registraron</strong> los datos on exíto.");
                                                $("#formRegistrar")[0].reset();
                                            } else {
                                                notificacion("alert-danger", "Error al <strong>registrar</strong> los datos.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }
                               
                                function ejecutarPostValidate() {
                                    if (operacion == 2)
                                        registrarPerfilProducto();
                                    // if (operacion == 2)
                                    operacion = null;
                                }
                                function notificacion(t, m) {
                                    if ($('#alert').text() == "") {
                                        setTimeout('$("#alert").text("")', 3000);
                                    }
                                    $("#alert").text("");
                                    $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                                            + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                            + '<span aria-hidden="true">×</span>'
                                            + '</button>'
                                            + m
                                            + '</div>');
                                    $("#alert").focus();

                                }                                
                                function listarSubtipoProducto(idCombo) {
                                    ajaxGestion.listarSubtipoProducto({
                                        callback: function (data) {
                                            if (data !== null) {
                                                dwr.util.removeAllOptions(idCombo);
                                                dwr.util.addOptions(idCombo, [{
                                                        id: '',
                                                        descripcion: 'Seleccione una opción'
                                                    }], 'id', 'descripcion');
                                                dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                                                //jQuery("#" + idCombo).val(valorSeleccionado);
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }

</script>