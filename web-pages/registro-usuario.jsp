<%-- 
    Document   : registro-usuario
    Created on : 24/06/2018, 06:01:08 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<h1 class="page-title">Registrar Investigador</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="form-basic.html">Usuario</a></li> 
    <li class="active"><strong>Registro</strong></li> 
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Registro</h4>
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <form role="form" id="formRegistrar" action="return:false" autocomplete="off">
                    <div id="alert"></div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required maxlength="50" title="Ingrese nombre">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="apellido">Apellido</label>
                            <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" required maxlength="50" title="Ingrese apellido">
                        </div>                     
                    </div> 
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="tUsuario">Tipo de usuario</label>
                            <select onchange="javascript:verTipoInvestigador(this.value);" class="form-control" id="tUsuario" name="tUsuario" required title="Seleccione una opcion"></select>
                        </div>
                        <div class="form-group col-lg-3" hidden id="divTinvestigador">                                                   
                            <label class="control-label" for="tInvestigador">Tipo de investigador</label>
                            <select onchange="javascript:listarSubTipoInvestigador(this.value, 'sInvestigador');" class="form-control" id="tInvestigador" name="tInvestigador" title="Seleccione una opcion"></select>
                        </div>
                        <div class="form-group col-lg-3" hidden id="divSinvestigador">                                                   
                            <label class="control-label" for="sInvestigador">Sudtipo de investigador</label>
                            <select class="form-control" id="sInvestigador" name="sInvestigador" title="Seleccione una opcion"></select>
                        </div>                         
                    </div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="documento">No. documento</label>
                            <input remote="ValidarDocumento" type="tel" class="form-control" id="documento" name="documento" placeholder="No. documento" required maxlength="10" title="Ingrese un documento que no este registrado">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="direccion">Dirección</label>
                            <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" required maxlength="100" title="Ingrese dirección">
                        </div>                     
                    </div> 
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="telefono">Teléfono</label>
                            <input type="tel" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" required maxlength="10" title="Ingrese teléfono">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="correo">Correo</label>
                            <input remote="validarUsuario" type="email" class="form-control" id="correo" name="correo" placeholder="Correo" required maxlength="50" title="Ingrese un correo que no este resgistrado" onkeyup="javascript:$('#usuario').val(this.value)">
                        </div>                     
                    </div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="sexo">Sexo</label>
                            <select class="form-control" id="sexo" name="sexo" required title="Seleccione una opcion">
                                <option value="">Seleccione una opción</option>
                                <option value="1">Hombre</option>
                                <option value="0">Mujer</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="estado">Estado</label>
                            <select class="form-control" id="estado" name="estado" required title="Seleccione una opcion">
                                <option value="">Seleccione una opción</option>
                                <option value="1">Acttivo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div> 
                    </div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="usuario">Usuario</label>
                            <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuario" disabled required maxlength="50" title="Ingrese usuario">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="contraseña">Contraseña</label>
                            <input type="password" class="form-control" id="contraseña" name="contraseña" placeholder="Contraseña" required minlength="4" maxlength="6" title="Ingrese contraseña">
                        </div>                     
                    </div>
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="reset" class="btn btn-white btn-rounded">Cancelar</button>
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formRegistrar', 1);">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<footer class="footer-main"> 
    &copy; 2016 <strong>Integral</strong> Admin Template by <a target="_blank" href="#/">G-axon</a>
</footer>	
<!-- /footer -->
<script>
    var operacion = null;
    $(document).ready(function () {
        listarTipoUsuario("tUsuario");
        listarTipoInvestigador("tInvestigador");
    });

    $("input[type=tel]").on('input', function () {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    function registrarUsuario() {

        var usuario = {
            usuario: $("#usuario").val(),
            clave: $("#contraseña").val(),
            estado: $("#estado").val(),
            idTipoUsuario: $("#tUsuario").val()
        };

        var perfil = {
            documento: $("#documento").val(),
            nombre: $("#nombre").val(),
            apellido: $("#apellido").val(),
            idSubtipoInvesstigador: $("#sInvestigador").val(),
            direccion: $("#direccion").val(),
            telefono: $("#telefono").val(),
            correo: $("#correo").val(),
            sexo: $("#sexo").val()
        };
        ajaxGestion.registrarUsuario(usuario, perfil, {
            callback: function (data) {
                console.log(data);
                if (data) {
                    notificacion("alert-success", "Se <strong>registraron</strong> los datos on exíto.");
                    $("#formRegistrar")[0].reset();
                } else {
                    notificacion("alert-danger", "Error al <strong>registrar</strong> los datos.");
                }
            },
            timeout: 20000
        });
    }

    function notificacion(t, m) {
        if ($('#alert').text() == "") {
            setTimeout('$("#alert").text("")', 3000);
        }
        $("#alert").text("");
        $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                + '<span aria-hidden="true">×</span>'
                + '</button>'
                + m
                + '</div>');
        $("#alert").focus();

    }

    function ejecutarPostValidate() {
        if (operacion == 1)
            registrarUsuario();
        // if (operacion == 2)
        operacion = null;
    }

    function listarTipoUsuario(idCombo) {
        ajaxGestion.listarTipoUsuario({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarSubTipoInvestigador(idTipoInv, idCombo, valorSeleccionado) {
        ajaxGestion.listarSubTipoInvestigador(idTipoInv, {
            callback: function (data) {
                dwr.util.removeAllOptions(idCombo);
                dwr.util.addOptions(idCombo, [{
                        id: '',
                        descripcion: 'Seleccione una opción'
                    }], 'id', 'descripcion');
                if (data !== null) {
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarTipoInvestigador(idCombo, valorSeleccionado) {
        ajaxGestion.listarTipoInvestigador({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    $("#" + idCombo).val(valorSeleccionado).trigger("change");
                }
            },
            timeout: 20000
        });
    }

    function verTipoInvestigador(id) {
        if (id == "2") {
            $("#divSinvestigador").show();
            $("#divTinvestigador").show();
            $('#sInvestigador').prop("required", true);
            $('#tInvestigador').prop("required", true);
        } else {
            $("#divSinvestigador").hide();
            $("#divTinvestigador").hide();
            $('#sInvestigador').prop("required", false);
            $('#tInvestigador').prop("required", false);
            $('#sInvestigador').val("");
            $('#tInvestigador').val("");
        }
    }
</script>